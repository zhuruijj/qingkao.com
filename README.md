# python利用dilb实现人脸识别和剪切

功能：进行人脸识别并对识别到的人脸生成单个图像保存到本地

内容如下：

利用 Python 开发，借助 Dlib 库进行人脸检测 / face detection 和剪切；

1.crop_faces_show.py
将检测到的人脸剪切下来, 依次排序平铺显示在新的图像上;

2.crop_faces_save.py
将检测到的人脸生成单个图像存储到本地路径;

实现过程： 
工作内容主要分为两大块：Dlib 人脸检测 和 处理检测到的人脸图像


一、Dlib 人脸检测　　

利用已经训练好的 Dlib 正向人脸检测器 detector = dlib.get_frontal_face_detector() 进行人脸检测；


可以得到人脸外接矩形的坐标，用来之后进行裁剪；


二、绘制新图像

让检测出来的人脸并排显示，需要遍历两次（ for k, d in enumerate (faces) ）:

第一次遍历：记录下需要生成的图像窗口的大小，因为需要将多张照片显示在一张图像上，所以需要知道每张人脸照片的大小；

第二次遍历：根据之前得到的图像尺寸新建空白图像，然后开始用人脸矩形填充图像。

（1）确定空白图像尺寸

这部分首先要根据检测到的人脸数和人脸大小，来确定绘制图像所需要的尺寸）　　

第一次遍历：多张人脸要输出到一行，所以先进行一次人脸的遍历j记下每张人脸的大小，记每张人脸的尺寸为 [ 高度 height  * 宽度 width ]（高度和宽度说明见 图像尺寸说明 ）
取生成空白图像的尺寸：height_max（最大高度）和 width_sum（宽度之和），然后根据尺寸大小来新建空白图像：

img_blank = np.zeros((height_max, width_sum, 3), np.uint8)

（2）图像填充

第二次遍历：多根据之前得到的图像尺寸新建空白图像，然后开始用人脸矩形填充图像，每次 width 方向从 blank_start 位置开始，每次填完一张之后记得更新起始位置：（ blank_start += width ）：

for i in range(height):


for j in range(width):


img_blank[i][blank_start + j] = img[d.top() + i][d.left() + j]

若访问图像的某点像素，利用 img [height] [width]：

存储像素是一个三维数组，先高度 height，然后宽度 width;

返回的是一个颜色数组（ 0-255，0-255，0-255 ），按照（ B, G, R ）的顺序；

如 蓝色 就是（255，0，0），红色 是（0，0，255）;